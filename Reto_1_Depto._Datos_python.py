#!/usr/bin/env python
# coding: utf-8

# ## Ejercicio 1
# 
# Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits that occur more than once in the input string. The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.
# 
# Example
# - "abcde" -> 0 # no characters repeats more than once
# - "aabbcde" -> 2 # 'a' and 'b'
# - "aabBcde" -> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
# - "indivisibility" -> 1 # 'i' occurs six times
# - "Indivisibilities" -> 2 # 'i' occurs seven times and 's' occurs twice
# - "aA11" -> 2 # 'a' and '1'
# - "ABBA" -> 2 # 'A' and 'B' each occur twice

# In[9]:


def duplicate_count(text):
    # Your code goes here
    text2 = text.lower()

    count = 0
    dictionary = {}

    for i in range(0,len(text2)):
        dictionary[text2[i]] = 0

    for key in dictionary:
        for i in range(0,len(text2)):
            if(key == text2[i]):
                dictionary[key] = dictionary[key] + 1

    for key in dictionary:
        if(dictionary[key] > 1):
            count += 1

    print(dictionary)
    return count

duplicate_count("abcde")
duplicate_count("abcdeaa")
# duplicate_count("abcdeaB")
# duplicate_count("Indivisibilities")


# ## Ejercicio 2
# 
# You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.
# 
# Examples
# 
# - [2, 4, 0, 100, 4, 11, 2602, 36]
# Should return: 11 (the only odd number)
# 
# - [160, 3, 1719, 19, 11, 13, -21]
# Should return: 160 (the only even number)

# In[ ]:


def find_outlier(integers):
    odd = []
    even = []
    for num in integers:
      if num % 2 != 0:
        odd.append(num)

      if num % 2 == 0:
        even.append(num)

    if len(odd) > len(even):
      print("The even numbers:", even)
      

    else:
      print("The odd numbers:", odd)
          
  


find_outlier([2, 4, 6, 8, 10, 3])
find_outlier([2, 4, 0, 100, 4, 11, 2602, 36])
find_outlier([160, 3, 1719, 19, 11, 13, -21])


# ## Ejercicio 5
# 
# Given an array of ones and zeroes, convert the equivalent binary value to an integer.
# 
# Eg: [0, 0, 0, 1] is treated as 0001 which is the binary representation of 1.
# 
# 
# Examples:
# 
# - Testing: [0, 0, 0, 1] ==> 1
# - Testing: [0, 0, 1, 0] ==> 2
# - Testing: [0, 1, 0, 1] ==> 5
# - Testing: [1, 0, 0, 1] ==> 9
# - Testing: [0, 0, 1, 0] ==> 2
# - Testing: [0, 1, 1, 0] ==> 6
# - Testing: [1, 1, 1, 1] ==> 15
# - Testing: [1, 0, 1, 1] ==> 11

# In[8]:


def binary_array_to_number(arr):
    decimal = 0
    
    for lugar, digit in enumerate(arr[::-1]):
        decimal += int(digit)* 2** lugar
        
    return decimal

binary_array_to_number([0,0,0,1])
#binary_array_to_number([0,0,1,0])


# In[ ]:




